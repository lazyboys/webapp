/*###############################
#Team:  3
#City:  London
#Name:
#   Lu Fan (646627)
#   Thanh Quang Minh Pham (633262)
#   Renji Luke Harold (671917)
#   Kian Boon Mark Lee (665893)
#   Md Akmal Hossain (662254)
#
###############################*/

//Tweet Analysis

function pieTweetSentiment()
{

   $.get('files/tweetSentiment.txt', function(data) {

    lines = data.split("\n");

    chartData = [];
    //temp = [];

    for (i = 0; i < lines.length; i++) {
        var label = lines[i].split("$?!$");
        var temp = [];
        temp.push(label[0]);
        temp.push(Number(label[1]));
        chartData.push(temp);
        //alert(chartData[i]);
        //break;
    }
    chartData.pop();
    //chartData.pop();
    //alert(chartData);

    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Tweet Sentiment'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            name: 'Language Share',
            data: chartData}]
    });

  }, "text");
}

function pieLanguageDistribution() {

$.get('files/languageDistribution.txt', function(data) {

    lines = data.split("\n");

    chartData = [];
    //temp = [];

    for (i = 0; i < lines.length; i++) {
        var label = lines[i].split("$?!$");
        var temp = [];
        temp.push(label[0]);
        temp.push(Number(label[1]));
        chartData.push(temp);
        //alert(chartData[i]);
        //break;
    }
    chartData.pop();

    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Language Distribution'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            name: 'Language Share',
            data: chartData}]
    });

  }, "text");
}

function graphSentimentByLanguage() {

$.get('files/sentimentRatingLang.txt', function(data) {

    lines = data.split("\n");

    languages = [];
    sentiment = [];

    for (i = 0; i < lines.length; i++) {
        var label = lines[i].split("$?!$");
  languages.push(label[0]);
  sentiment.push(Number(label[1]));
    }

    $('#container').highcharts({

        chart: {
            type: 'column'
        },
        title: {
            text: 'Language Sentiment'
        },
        xAxis: {
           
      categories: languages
        },
        credits: {
            enabled: false
        },
        series: [{name: 'Sentiment',data:sentiment}]
    });

   }, "text");

}
//---------------------------------------------------------------------

//Prolific Tweeter

function graphProlificTweeter(){

  $.get('files/prolificTweeter.txt', function(data) {

    lines = data.split("\n");

    tweeterid = [];
    tweetcount = [];

    for (i = 0; i < lines.length; i++) {
        var label = lines[i].split("$?!$");
  tweeterid.push(label[1]);
  tweetcount.push(Number(label[0]));

    }

    $('#container').highcharts({

        chart: {
            type: 'column'
        },
        title: {
            text: 'Top 10 Tweeters'
        },
        xAxis: {
           
      categories: tweeterid
        },
        credits: {
            enabled: false
        },
        series: [{name: 'Tweet Count',data:tweetcount}]
    });

   }, "text");

}

function mapTrackUser(){

    $("#container").replaceWith(divClone.clone());

    var pointList = [];

    $.get('files/followProlificTweeter.txt', function(data) {

        var map = L.map("container").setView([51.505, -0.09], 13); 
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map); 
        
        lines = data.split("\n");

        for (var i = 0; i < lines.length; i++) {    
            var label = lines[i].split("$?!$");
            //alert(label);

            marker = new L.marker([label[0],label[1]])            
                .addTo(map);
            // var pointA = new L.LatLng(label[0], label[1]);
            // pointList.push(pointA);
            // var polyline = L.polyline(pointList, {color: 'red'}).addTo(map);
        }

        /*for (var i = 0; i < lines.length; i++) {
            alert(pointList);
            
        } */       

    }, "text");
}

//-----------------------------------------------------------------------------------
//Tourist Spots

function graphTouristTweetCount(){
    $.get('files/touristAttraction.txt', function(data) {
        
    lines = data.split("\n");

   touristname = []
   tweetcount = []

    for (i = 0; i < lines.length; i++) {
        var label = lines[i].split("$?!$");
          touristname.push(label[0]);
          tweetcount.push(Number(label[1]));

    }
    
    $('#container').highcharts({

        chart: {
            type: 'column'
        },
        title: {
            text: 'Tweet Count - Top Tourist Attractions'
        },
        xAxis: {
           
      categories: touristname
        },
        credits: {
            enabled: false
        },
        series: [{name: 'Tweet Count',data:tweetcount}]
    });

   }, "text");
}

function graphTouristTweetSentiment(){ 

    $.get('files/touristAttraction.txt', function(data) {

        lines = data.split("\n");

        var sentiment = []
        

        for (var i = 0; i < lines.length; i++) {    
            var label = lines[i].split("$?!$");           
            sentiment.push(Number(label[2]));
        }

        $('#container').highcharts({
            title: {
                text: 'Sentiment Variation - Top Tourist Attractions',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: Twitter.com',
                x: -20
            },
            xAxis: {
                categories: ['Buckingham Palace', 'Harrods', 'Hyde Park', 'London Eye', 'Southbank', 'Tower Bridge']
            },
            yAxis: {
                title: {
                    text: 'Sentiment'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            // tooltip: {
            //     valueSuffix: '°C'
            // },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Sentiment',
                data: sentiment
            }]
        });
    }, "text");
}

//-------------------------------------------------------------------------------------

//Soccer
 function mapSoccerFan(){
   
    var chelseaLoc = []
    var arsenalLoc = []

    $("#container").replaceWith(divClone.clone());

    var map = L.map("container").setView([51.505, -0.09], 10); 

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

    //icon
    var myIcon = L.icon({
        iconUrl: '../img/data/chelsea.png',
        iconSize: [38, 95],
        iconAnchor: [22, 94],
        popupAnchor: [-3, -76]
    });

    $.get('files/soccerFanLocation.txt', function(data) {

        lines = data.split("\n");
        var tempChelseaList = []
        var tempArsenalList = []

        for (var i = 0; i < lines.length; i++) {    
            var label = lines[i].split("$?!$");

            if(label[0] == "Chelsea Fan"){
                tempChelseaList.push(label[1]);
                tempChelseaList.push(label[2]);
                tempChelseaList.push(label[3]);
                //tempChelseaList.push(i+1);
                //alert(tempChelseaList);
            } else if (label[0] == "Arsenal Fan"){

                tempArsenalList.push(label[1]);
                tempArsenalList.push(label[2]);
                tempArsenalList.push(label[3]);
                //tempChelseaList.push(i+1);
                //alert(tempArsenalList);
            }

            chelseaLoc.push(tempChelseaList);
            arsenalLoc.push(tempArsenalList);
        }

        var arsenalMarker = L.icon({
            iconUrl: "../img/data/arsenal.png",
        });

        var chelseaMarker = L.icon({
            iconUrl: "../img/data/chelsea.png",

        });


        for (var i = 0; i < tempChelseaList.length; i++) {  
            marker = new L.marker([tempChelseaList[i+1],tempChelseaList[i+2]])
                .bindPopup(tempChelseaList[i])
                .addTo(map);
            marker.setIcon(chelseaMarker);
            i=i+2;
        }

        for (var i = 0; i < tempArsenalList.length; i++) {  
            marker = new L.marker([tempArsenalList[i+1],tempArsenalList[i+2]])
                .bindPopup(tempChelseaList[i])
                .addTo(map);
            marker.setIcon(arsenalMarker);
            i=i+2;
        }


        //for (var i = 0; i < lines.length; i++) {    
        //    var label = lines[i].split("$?!$");
        //alert(label);

        //    marker = new L.marker([label[1],label[2]],{icon: myIcon})            
        //        .bindPopup(label[0])
        //        .addTo(map);
        //}    

    }, "text");


    //add red circle onto map
    var circle = L.circle([51.59232, -0.15106], 8000, {
       color: 'red',
       fillColor: '#f03',
       fillOpacity: 0.5
    }).addTo(map);

    //add red circle onto map
    var circle = L.circle([51.48755, -0.15827], 3500, {
       color: 'blue',
       fillColor: '#3366FF',
       fillOpacity: 0.5
    }).addTo(map);

    // display popup long n lang when click on map
    var popup = L.popup();

    function onMapClick(e) { 
        popup
            .setLatLng(e.latlng)
            .setContent("You clicked the map at " + e.latlng.toString())
            .openOn(map);
    }

    map.on('click', onMapClick);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);  
}

function graphSoccerTimelineSentiment(){

    $.get('files/soccerSentimentTimeline.txt', function(data) {

        lines = data.split("\n");

        var chelsea = []
        var arsenal = []

        for (var i = 0; i < lines.length; i++) {    
            var label = lines[i].split("$?!$");

            if(label[4] == "Chelsea"){
                chelsea.push(Number(label[5]));
            } else if(label[4] == "Arsenal"){
                arsenal.push(Number(label[5]));
            }

        }

        $('#container').highcharts({
            title: {
                text: 'Sentiment Variation - Chelsea VS Arsenal',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: Twitter.com',
                x: -20
            },
            xAxis: {
                categories: ['10am', '11am', '12am', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm']
            },
            yAxis: {
                title: {
                    text: 'Sentiment'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            // tooltip: {
            //     valueSuffix: '°C'
            // },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Chelsea',
                data: chelsea
            }, {
                name: 'Arsenal',
                data: arsenal
            }]
        });
    }, "text");
}

function graphSoccerTimelineCount(){

    $.get('files/soccerSentimentTimeline.txt', function(data) {

        lines = data.split("\n");

        var chelsea = []
        var arsenal = []

        for (var i = 0; i < lines.length; i++) {    
            var label = lines[i].split("$?!$");

            if(label[4] == "Chelsea"){
                chelsea.push(Number(label[6]));
            } else if(label[4] == "Arsenal"){
                arsenal.push(Number(label[6]));
            }

        }

        $('#container').highcharts({
            title: {
                text: 'Tweet Count Variation - Chelsea VS Arsenal',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: Twitter.com',
                x: -20
            },
            xAxis: {
                categories: ['10am', '11am', '12am', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm']
            },
            yAxis: {
                title: {
                    text: 'Sentiment'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            // tooltip: {
            //     valueSuffix: '°C'
            // },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Chelsea',
                data: chelsea
            }, {
                name: 'Arsenal',
                data: arsenal
            }]
        });
    }, "text");
}

//----------------------------------------------------------------------------------

//Election
function graphUKElection(){

  $.get('files/electionSentimentCumulative.txt', function(data) {

        lines = data.split("\n");

        var positive = [Number((lines[0].split("$?!$"))[1]),Number((lines[1].split("$?!$"))[1])];
        var neutral = [Number((lines[0].split("$?!$"))[2]),Number((lines[1].split("$?!$"))[2])];
        var negative = [Number((lines[0].split("$?!$"))[3]),Number((lines[1].split("$?!$"))[3])];
        var cumulative = [Number((lines[0].split("$?!$"))[4]),Number((lines[1].split("$?!$"))[4])];
    

    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'UK Election Candidate Popularity'
        },
        xAxis: {
            categories: ['David Cameron', 'Ed Miliband']
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Postive',
            data: positive
        }, {
            name: 'Neutral',
            data: neutral
        }, {
            name: 'Negative',
            data: negative
        }, {
            name: 'Cumulative',
            data: cumulative
        }]
    });

    }, "text");
}

function graphElectionTimeline(){

    $.get('files/candidateTimeline.txt', function(data) {

        lines = data.split("\n");

        var cameron = []
        var miliband = []
        for (var i = 0; i < lines.length; i++) {    
            var label = lines[i].split("$?!$");

            if(label[1] == "David Cameron"){
                cameron.push(Number(label[2]));
            } else if(label[1] == "Ed miliband"){
                miliband.push(Number(label[2]));
            }

        }

        $('#container').highcharts({
            title: {
                text: 'Sentiment Variation of UK Election ',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: Twitter.com',
                x: -20
            },
            xAxis: {
                categories: ['1 April', '2 April', '3 April', '4 April', '5 April', '6 April', '7 April', '8 April', '9 April', '10 April', 
                '11 April', '12 April', '13 April', '14 April', '15 April', '16 April', '17 April', '18 April', '19 April', '20 April', 
                '21 April', '22 April', '23 April', '24 April', '25 April', '26 April', '27 April', '28 April', '29 April', '30 April', 
                '1 May', '2 May', '3 May', '4 May', '5 May', '6 May', '7 May', '8 May', '9 May', '10 May', '11 May', '12 May', '13 May']
            },
            yAxis: {
                title: {
                    text: 'Sentiment'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            // tooltip: {
            //     valueSuffix: '°C'
            // },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'David Cameron',
                data: cameron
            }, {
                name: 'Ed Miliband',
                data: miliband
            }]
        });
    }, "text");
}

function graphElectionSupporterCount(){
    
    var hater = []
    var supporter = []
    var swing = []

    $.get('files/candidatePopularity.txt', function(data) {

        lines = data.split("\n");
        
        hater     = [Number((lines[0].split("$?!$"))[1]), Number((lines[3].split("$?!$"))[1])];
        supporter = [Number((lines[1].split("$?!$"))[1]), Number((lines[4].split("$?!$"))[1])];
        swing     = [Number((lines[2].split("$?!$"))[1]), Number((lines[5].split("$?!$"))[1])];
        
    

// alert(hater);
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Popularity of Candidates'
        },
        subtitle: {
            text: 'Source: Twitter.com'
        },
        xAxis: {
            categories: ['David Cameron', 'Ed Miliband'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No.of Tweets',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },        
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Supporter',
            data: supporter
        }, {
            name: 'Hater',
            data: hater
        }, {
            name: 'Swing',
            data: swing
        }]
    });
    }, "text");
}
function mapElectionSupporter(){
   
    // var cameron = []
    // var miliband = []

    $("#container").replaceWith(divClone.clone());

    var map = L.map("container").setView([51.505, -0.09], 13); 

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

    //icon
    // var myIcon = L.icon({
    //     iconUrl: 'http://harrywood.co.uk/maps/examples/leaflet/leaflet/images/marker.png',
    //     iconSize: [38, 95],
    //     iconAnchor: [22, 94],
    //     popupAnchor: [-3, -76]
    // });

    $.get('files/candidateFanLocation.txt', function(data) {

        lines = data.split("\n");
        var tempCameronList = []
        var tempMilibandList = []

        for (var i = 0; i < lines.length; i++) {    
            var label = lines[i].split("$?!$");

            if(label[0] == "David Cameron"){
                tempCameronList.push(label[1]);
                tempCameronList.push(label[2]);
                tempCameronList.push(label[3]);
                //tempChelseaList.push(i+1);
                //alert(tempChelseaList);
            } else if (label[0] == "Ed miliband"){
                tempMilibandList.push(label[1]);
                tempMilibandList.push(label[2]);
                tempMilibandList.push(label[3]);
                //tempChelseaList.push(i+1);
                //alert(tempArsenalList);
            }

            // chelseaLoc.push(tempChelseaList);
            // arsenalLoc.push(tempArsenalList);
        }

        // var cameronMarker = L.icon({
        //     iconUrl: "../img/data/conservatives.jpg",
        // });

        // var milibandMarker = L.icon({
        //     iconUrl: "../img/data/labour.jpg",

        // });

        var redMarker = L.icon({
            iconUrl: "http://harrywood.co.uk/maps/examples/leaflet/marker-icon-red.png",  
        });

        var blueMarker = L.icon({
            iconUrl: "http://harrywood.co.uk/maps/examples/leaflet/leaflet/images/marker.png",  
        });

        for (var i = 0; i < tempCameronList.length; i++) { 
            marker = new L.marker([tempCameronList[i+1],tempCameronList[i+2]])
                .bindPopup(tempCameronList[i])
                .addTo(map);
            marker.setIcon(blueMarker);
            i=i+2;
        }

        for (var i = 0; i < tempMilibandList.length; i++) { 
            marker = new L.marker([tempMilibandList[i+1],tempMilibandList[i+2]])
                .bindPopup(tempMilibandList[i])
                .addTo(map);
            marker.setIcon(redMarker);
            i=i+2;
        }


        //for (var i = 0; i < lines.length; i++) {    
        //    var label = lines[i].split("$?!$");
        //alert(label);

        //    marker = new L.marker([label[1],label[2]],{icon: myIcon})            
        //        .bindPopup(label[0])
        //        .addTo(map);
        //}    

    }, "text");


    //add red circle onto map
    //var circle = L.circle([51.42613, 0.3447], 12500, {
    //    color: 'red',
    //    fillColor: '#f03',
    //    fillOpacity: 0.5
    //}).addTo(map);

    //add red circle onto map
    //var circle = L.circle([51.42613, 0.3447], 12500, {
    //    color: 'red',
    //    fillColor: '#f03',
    //    fillOpacity: 0.5
    //}).addTo(map);

    // display popup long n lang when click on map
    var popup = L.popup();

    function onMapClick(e) { 
        popup
            .setLatLng(e.latlng)
            .setContent("You clicked the map at " + e.latlng.toString())
            .openOn(map);
    }

    map.on('click', onMapClick);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);  
}







